package name.luoyong.hibernate.entitymanager;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import name.luoyong.hibernate.basic.entity.Person;
import name.luoyong.hibernate.demoMapp.Demo;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;

import name.luoyong.hibernate.basic.entity.manyToOne.Group;
import name.luoyong.hibernate.basic.entity.manyToOne.User;

public class App {

    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");

    public static void main(String[] args) {

       // persist();
         //find();
        // getReference();
        // remove();
        // queryResultList();
        // querySingleResult();
        // nativeQuery();
        // namedQuery();
        // testGetUserByFields();
        //getPerson();
        //Demo.main();
        //Demo.main2();
       //Demo.main3();
      //  Demo.demoHeritage();
       // Demo.demoHeritage2();
        //Demo.searchBook();
        Demo.main2();

        entityManagerFactory.close();

    }

    private static void persist() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        User ly = new User();
        ly.setUsername("moi");
        ly.setPassword("pipi");

        Group group = new Group();
        group.setName("second2");

        ly.setGroup(group);
        group.getUsers().add(ly);

        entityManager.persist(ly);
        entityManager.persist(group);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static void find() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        //User user = entityManager.find(User.class, 1L);
        // User user2 = entityManager.getReference(User.class, 1L);

        Group group = entityManager.find(Group.class, 1L);
       // System.out.println(group.getUsers());

        //System.out.println(user.getUsername());
       // System.out.println(user.getPassword());

        // entityManager.clear();
        // entityManager.merge(user);


        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static void getReference() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        User user = entityManager.getReference(User.class, 1L);


        user.setPassword("se");

        System.out.println(user.getUsername());
        System.out.println(user.getPassword());

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static void remove() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        User user = entityManager.find(User.class, 1L);
        entityManager.remove(user);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static void queryResultList() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager.createQuery("from User");
        List<User> users = query.getResultList();


        for (User user : users) {
            System.out.println(user.getUsername());
            System.out.println(user.getGroup().getName());
        }

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static void querySingleResult() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<User> query = entityManager.createQuery("from User where username=:username", User.class);
        query.setParameter("username", "'moi'");
        User user = query.getSingleResult();

        System.out.println(user.getUsername());
        System.out.println(user.getGroup().getName());

        entityManager.getTransaction().commit();
        entityManager.close();
    }


    private static void nativeQuery() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager.createNativeQuery("select username, password from user2");
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        List<Map> users = query.getResultList();

        for (Map userMap : users) {
            System.out.println(userMap.get("username"));

        }

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static void namedQuery() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager.createQuery("from User");
        entityManagerFactory.addNamedQuery("fromUser", query);

        Query query2 = entityManager.createNamedQuery("fromUser");

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static void testGetUserByFields() {
        User user = getUserByFields("version", 1);
        System.out.println(user.getUsername());
        System.out.println(user.getPassword());
    }

    private static User getUserByFields(Object... params) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        String sql = "from User where ";

        int paramsLength = params.length;

        int index = 0;
        while (index < paramsLength) {
            sql += " " + params[index] + "=:" + params[index];
            index += 2;
        }

        TypedQuery<User> query = entityManager.createQuery(sql, User.class);

        index = 0;
        while (index < paramsLength) {
            query.setParameter((String) params[index], params[index + 1]);
            index += 2;
        }

        User user = query.getSingleResult();

        entityManager.getTransaction().commit();
        entityManager.close();

        return user;
    }


    private static void getPerson() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        List<Person> individus = null;
        individus = entityManager.createNativeQuery("select * from person", Person.class)
                .getResultList();

        individus.stream().forEach(System.out::println);

        entityManager.getTransaction().commit();
        entityManager.close();

    }



	/*int ageMax = 25;
	List<Individu> individus = null;
	individus = entityManager
			.createNativeQuery("select * from individu where age <= ?", Individu.class)
			.setParameter(1, ageMax)
              .getResultList();*/

	/*long result = (Long) entityManager
			.createNativeQuery("select count(1) from individu")
			.getSingleResult();*/


	/*long individuId = 1;
// Cette requête nécessite une transaction active
entityManager.createNativeQuery("delete from individu where individuId = ?")
		.setParameter(1, individuId)
             .executeUpdate();*/

// Les requêtes JPQL


	/*List<Individu> individus = null;
	individus = entityManager.createQuery("select i from Individu i", Individu.class)
			.getResultList();
*/



	/*int ageMax = 25;
	List<Individu> individus = null;
	individus = entityManager
			.createQuery("select i from Individu i where i.age <= :ageMax", Individu.class)
			.setParameter("ageMax", ageMax)
                .getResultList();*/


	/*long result = (Long) entityManager.createQuery("select count(i) from Individu i")
			.getSingleResult();*/

	/*long individuId = 1;
// Cette requête nécessite une transaction active
entityManager.createQuery("delete from Individu i where i.id = :id")
		.setParameter("id", individuId)
             .executeUpdate();*/

}
