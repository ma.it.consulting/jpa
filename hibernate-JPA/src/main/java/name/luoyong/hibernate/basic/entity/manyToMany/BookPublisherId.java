package name.luoyong.hibernate.basic.entity.manyToMany;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class BookPublisherId implements Serializable {

    @Column(name = "book_id")
    private Integer bookId;

    @Column(name = "publisher_id")
    private Integer publisherId;

    public BookPublisherId(){

    }

    public BookPublisherId(Integer bookId, Integer publisherId) {
        this.bookId = bookId;
        this.publisherId = publisherId;
    }
}