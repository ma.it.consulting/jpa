package name.luoyong.hibernate.basic.entity;

import javax.persistence.*;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @Column(name = "personId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic
    @Column(length = 30, nullable = false)
    private String nom;

    @Basic
    @Column(length = 30, nullable = false)
    private String prenom;

    @Column(length = 3, nullable = false)
    private Integer age;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", age=" + age +
                '}';
    }
}
