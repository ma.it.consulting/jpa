package name.luoyong.hibernate.basic.entity.heritage2;

import javax.persistence.Entity;

@Entity
public class Chat extends Animal {


    private String crie;


    public Chat(){

    }

    public Chat(String crie) {
        this.crie = crie;
    }

    public Chat(String nom, String crie) {
        super(nom);
        this.crie = crie;
    }


    public String getCrie() {
        return crie;
    }

    public void setCrie(String crie) {
        this.crie = crie;
    }
}
