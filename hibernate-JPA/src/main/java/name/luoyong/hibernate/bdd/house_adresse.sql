use bdd_jpa;

create table house(
id int primary key auto_increment,
taille int,
address_id int,
foreign key (address_id)references address(id)
);

create table address(
id int primary key auto_increment,
numero int not null,
nom_rue varchar(100),
code_postal varchar(5) not null,
ville varchar(100)
);