package org.example.entity.oneToMany;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="group2")
public class Group {


    private Long id;

    private String name;

    private Set<User> users = new HashSet<>();


    public Group(){

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
