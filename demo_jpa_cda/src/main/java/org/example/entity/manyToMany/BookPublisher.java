package org.example.entity.manyToMany;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name ="book_publisher")
public class BookPublisher {

    @EmbeddedId
    private BookPublisherId bookPublisherId;

    @ManyToOne
    @MapsId("bookId")
    @JoinColumn(name="book_id")
    private Book book;

    @ManyToOne
    @MapsId("publisherId")
    @JoinColumn(name="publisher_id")
    private Publisher publisher;

    @Column(name="published_date")
    private Date publishedDate;

    public BookPublisher(){

    }

    public BookPublisher(BookPublisherId bookPublisherId, Book book, Publisher publisher, Date publishedDate) {
        this.bookPublisherId = bookPublisherId;
        this.book = book;
        this.publisher = publisher;
        this.publishedDate = publishedDate;
    }
}
