create database banque_bdd;

use banque_bdd;

create table client(
id int primary key auto_increment,
nom varchar(50),
prenom varchar(50),
naissance_d date,
id_agence int,
foreign key (id_agence) references agence(id)
);

create table compte(
id int primary key auto_increment,
libelle varchar(50) not null,
iban varchar(27) not null,
solde double(10,2)
);

create table agence(
id int primary key auto_increment,
adresse varchar(255)
);

create table client_compte(
client_id int,
compte_id int,
primary key (client_id,compte_id),
foreign key (client_id) references client(id),
foreign key (compte_id) references compte(id)
);