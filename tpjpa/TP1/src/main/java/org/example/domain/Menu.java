package org.example.domain;

import org.example.entity.Agence;
import org.example.entity.Client;
import org.example.entity.Compte;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_tp1");
    private static EntityManager em = emf.createEntityManager();


    public static void aff_menu(){
        System.out.println("1 - Créer tout");
        System.out.println("2 - Créer un compte");
        System.out.println("3 - Exit");
    }

    public static void crea_banque(){


        Scanner sc = new Scanner(System.in);
        System.out.println("Entrer l'adresse de l'agence");
        String adresse = sc.nextLine();
        Agence agence = new Agence();
        agence.setAdresse(adresse);
        em.persist(agence);


    }

    public static Client create_client(){
        Client client = new Client();
        Scanner sc = new Scanner(System.in);


        try {
            System.out.println("Entrer le nom");
            String nom = sc.next();
            System.out.println("Entrer le prénom");
            String prenom = sc.next();
            System.out.println("Entrer la date de naissance (format YYYY-MM-DD)");
            String date_s = sc.next();
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(date_s);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

            client.setNom(nom);
            client.setPrenom(prenom);
            client.setNaissance_d(date);



        }catch (InputMismatchException ex)
        {
            System.out.println("données invalide, réessayer");
            create_client();
        }
        return client;
    }

//    public static Client findClientById(Long id){
//
//        em.getTransaction().begin();
//       Client client= em.find(Client.class,id);
//       em.getTransaction().commit();
//
//       return client;
//
//    }
    public static Agence findAgenceById(Long id){


        Agence agence= em.find(Agence.class,id);


        return agence;
    }

    public static void create_compte(){
        Scanner sc = new Scanner(System.in);

        try{



            Client client = create_client();


            System.out.println("Entrer l'id de l'Agence où est lié le compte");
            Long id_a = sc.nextLong();
            Agence agence = findAgenceById(id_a);

            if(agence ==null) {
                System.out.println("Il n'y a pas d'agence à cette id , réessayer");
                create_compte();
            }else{
                System.out.println("Entrer le libellé du compte");
                sc.nextLine();
                String libelle = sc.nextLine();
                System.out.println("Entrer l'IBAN");
                String iban = sc.next();
                System.out.println("Entrer le solde du compte");
                double solde = sc.nextDouble();
                Compte compte = new Compte();
                compte.setLibelle(libelle);
                compte.setIban(iban);
                compte.setSolde(solde);
                compte.setAgence(agence);
                compte.getClients().add(client);
                em.persist(compte);



            }


        }catch (InputMismatchException ex){
            System.out.println("Type de données incorrectes réessayer");
        }
    }
    public static void exo(){
        em.getTransaction().begin();
        Scanner sc = new Scanner(System.in);
        int choix=0;
        do{
            try{
                aff_menu();
                choix = sc.nextInt();
                switch (choix){
                    case 1:
                        creation();
                        break;
                    case 2:
                        create_compte();
                        break;
                    case 3:
                        System.out.println("Aurevoir");
                        em.getTransaction().commit();
                        em.close();
                        emf.close();

                    default:
                        System.out.println("Commande invalide réessayer");
                        break;
                }

            }catch (InputMismatchException ex){
                System.out.println("Veuillez entrer un entier");
                aff_menu();
            }

        }while(choix !=3);
    }

    public static void creation(){
        Scanner sc = new Scanner(System.in);
        Client client = new Client();
        try{
            System.out.println("Entrer l'adresse de l'agence");
            sc.nextLine();
            String adresse = sc.nextLine();
            Agence agence = new Agence();
            agence.setAdresse(adresse);
            em.persist(agence);
            System.out.println("Entrer le nom");
            String nom = sc.next();
            System.out.println("Entrer le prénom");
            String prenom = sc.next();
            System.out.println("Entrer la date de naissance (format YYYY-MM-DD)");
            String date_s = sc.next();
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(date_s);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

            client.setNom(nom);
            client.setPrenom(prenom);
            client.setNaissance_d(date);
            em.persist(client);

            if(agence ==null) {
                System.out.println("Il n'y a pas d'agence à cette id , réessayer");
                create_compte();
            }else {
                System.out.println("Entrer le libellé du compte");
                sc.nextLine();
                String libelle = sc.nextLine();
                System.out.println("Entrer l'IBAN");
                String iban = sc.next();
                System.out.println("Entrer le solde du compte");
                double solde = sc.nextDouble();
                Compte compte = new Compte();
                compte.setLibelle(libelle);
                compte.setIban(iban);
                compte.setSolde(solde);
                compte.setAgence(agence);
                compte.getClients().add(client);
                client.getComptes().add(compte);
                em.persist(compte);

            }
        }catch (InputMismatchException ex){
            System.out.println("erreur de typage de données");
        }
    }
}
