package org.example.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "pays")
public class Pays {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_p;

    @Column(length = 50)
    private String nom_p;

    @OneToMany(mappedBy = "pays",cascade = CascadeType.ALL)
    List<Auteur> auteurList = new ArrayList<>();

    public Long getId_p() {
        return id_p;
    }

    public void setId_p(Long id_p) {
        this.id_p = id_p;
    }

    public String getNom_p() {
        return nom_p;
    }

    public List<Auteur> getAuteurList() {
        return auteurList;
    }

    public void setAuteurList(List<Auteur> auteurList) {
        this.auteurList = auteurList;
    }

    public void setNom_p(String nom_p) {
        this.nom_p = nom_p;
    }

    public Pays(Long id_p, String nom_p) {
        this.id_p = id_p;
        this.nom_p = nom_p;
    }

    public Pays(String nom_p) {
        this.nom_p = nom_p;
    }

    public Pays() {
    }

    @Override
    public String toString() {
        return "Pays{" +
                "id_p=" + id_p +
                ", nom_p='" + nom_p + '\'' +
                '}';
    }
}
