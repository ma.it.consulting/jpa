package org.example.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "typelivre")
public class TypeLivre {

    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_t")
    private Long id_t;

    @Column(length = 30)
    private String libelle_t;

    @OneToMany(mappedBy = "typeLivre")
    List<Livre> livreList = new ArrayList<>();

    public Long getId_t() {
        return id_t;
    }

    public void setId_t(Long id) {
        this.id_t = id;
    }

    public String getLibelle_t() {
        return libelle_t;
    }

    public void setLibelle_t(String libelle_t) {
        this.libelle_t = libelle_t;
    }

    public List<Livre> getLivreList() {
        return livreList;
    }

    public void setLivreList(List<Livre> livreList) {
        this.livreList = livreList;
    }

    public TypeLivre(Long id_t, String libelle_t) {
        this.id_t = id_t;
        this.libelle_t = libelle_t;
    }

    public TypeLivre(String libelle_t) {
        this.libelle_t = libelle_t;
    }

    public TypeLivre() {
    }
}
