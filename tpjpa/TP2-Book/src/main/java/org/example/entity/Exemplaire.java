package org.example.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class Exemplaire {

    @Id
    private String ref_e;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_ed")
    private Edition edition;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_l")
    private Livre livre;

    @OneToMany(mappedBy = "exemplaire",cascade = CascadeType.ALL, orphanRemoval = true)
    List<Emprunt> empruntList = new ArrayList<>();

    public String getRef_e() {
        return ref_e;
    }

    public void setRef_e(String ref_e) {
        this.ref_e = ref_e;
    }

    public Edition getEdition() {
        return edition;
    }

    public void setEdition(Edition edition) {
        this.edition = edition;
    }

    public Livre getLivre() {
        return livre;
    }

    public void setLivre(Livre livre) {
        this.livre = livre;
    }

    public List<Emprunt> getEmpruntList() {
        return empruntList;
    }

    public void setEmpruntList(List<Emprunt> empruntList) {
        this.empruntList = empruntList;
    }

    public Exemplaire(String ref_e, Edition edition, Livre livre) {
        this.ref_e = ref_e;
        this.edition = edition;
        this.livre = livre;
    }

    public Exemplaire() {
    }

    @Override
    public String toString() {
        return "Exemplaire{" +
                "ref_e='" + ref_e + '\'' +
                ", edition=" + edition +
                ", livre=" + livre +
                '}';
    }
}
