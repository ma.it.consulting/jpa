package org.example.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "livre")
public class Livre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_l;

    @Column(length = 254)
    private String titre_l;

    @Column(length = 4)
    private String annee_l;

    @Column( length = 2000)
    private String resume_l;

    @ManyToOne
    @JoinColumn(name = "id_t")
    private TypeLivre typeLivre;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "redaction",
            joinColumns = @JoinColumn(name = "id_l"),
            inverseJoinColumns = @JoinColumn(name = "id_a"))
    private List<Auteur> auteurList = new ArrayList<>();

    @OneToMany(mappedBy = "livre",fetch = FetchType.LAZY)
    private List<Exemplaire> exemplaireList = new ArrayList<>();

    public Long getId_l() {
        return id_l;
    }

    public void setId_l(Long id_l) {
        this.id_l = id_l;
    }

    public String getTitre_l() {
        return titre_l;
    }

    public void setTitre_l(String titre_l) {
        this.titre_l = titre_l;
    }

    public String getAnnee_l() {
        return annee_l;
    }

    public void setAnnee_l(String annee_l) {
        this.annee_l = annee_l;
    }

    public String getResume_l() {
        return resume_l;
    }

    public void setResume_l(String resume_l) {
        this.resume_l = resume_l;
    }

    public TypeLivre getTypeLivre() {
        return typeLivre;
    }

    public void setTypeLivre(TypeLivre typeLivre) {
        this.typeLivre = typeLivre;
    }

    public List<Auteur> getAuteurList() {
        return auteurList;
    }

    public void setAuteurList(List<Auteur> auteurList) {
        this.auteurList = auteurList;
    }

    public List<Exemplaire> getExemplaireList() {
        return exemplaireList;
    }

    public void setExemplaireList(List<Exemplaire> exemplaireList) {
        this.exemplaireList = exemplaireList;
    }

    public Livre (String titre_l, String annee_l, String resume_l, TypeLivre typeLivre, List<Auteur> auteurList, List<Exemplaire> exemplaireList) {
        this.titre_l = titre_l;
        this.annee_l = annee_l;
        this.resume_l = resume_l;
        this.typeLivre = typeLivre;
        this.auteurList = auteurList;
        this.exemplaireList = exemplaireList;
    }

    public Livre() {
    }
}
