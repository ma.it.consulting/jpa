package org.example.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "edition")
public class Edition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_ed;

    @Column(length = 254)
    private String nom_ed;

    @OneToMany(mappedBy = "edition")
    private List<Exemplaire> exemplaireList = new ArrayList<>();

    public Long getId_ed() {
        return id_ed;
    }

    public void setId_ed(Long id_ed) {
        this.id_ed = id_ed;
    }

    public String getNom_ed() {
        return nom_ed;
    }

    public void setNom_ed(String nom_ed) {
        this.nom_ed = nom_ed;
    }

    public List<Exemplaire> getExemplaireList() {
        return exemplaireList;
    }

    public void setExemplaireList(List<Exemplaire> exemplaireList) {
        this.exemplaireList = exemplaireList;
    }

    public Edition(String nom_ed, List<Exemplaire> exemplaireList) {
        this.nom_ed = nom_ed;
        this.exemplaireList = exemplaireList;
    }

    public Edition() {
    }
}
