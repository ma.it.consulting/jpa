package org.example.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "emprunt")
public class Emprunt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_em;

    private Date date_em;

    @Column(columnDefinition = "integer default 0")
    private Integer delais_em;

    @ManyToOne
    @JoinColumn(name = "id_i")
    private Inscrit inscrit;

    @ManyToOne
    @JoinColumn(name = "ref_e")
    private Exemplaire exemplaire;

    public Long getId_em() {
        return id_em;
    }

    public void setId_em(Long id_em) {
        this.id_em = id_em;
    }

    public Date getDate_em() {
        return date_em;
    }

    public void setDate_em(Date date_em) {
        this.date_em = date_em;
    }

    public Integer getDelais_em() {
        return delais_em;
    }

    public void setDelais_em(Integer delais_em) {
        this.delais_em = delais_em;
    }

    public Inscrit getInscrit() {
        return inscrit;
    }

    public void setInscrit(Inscrit inscrit) {
        this.inscrit = inscrit;
    }

    public Exemplaire getExemplaire() {
        return exemplaire;
    }

    public void setExemplaire(Exemplaire exemplaire) {
        this.exemplaire = exemplaire;
    }

    public Emprunt(Long id_em, Date date_em, Integer delais_em, Inscrit inscrit, Exemplaire exemplaire) {
        this.id_em = id_em;
        this.date_em = date_em;
        this.delais_em = delais_em;
        this.inscrit = inscrit;
        this.exemplaire = exemplaire;
    }

    public Emprunt(Date date_em, Integer delais_em, Inscrit inscrit, Exemplaire exemplaire) {
        this.date_em = date_em;
        this.delais_em = delais_em;
        this.inscrit = inscrit;
        this.exemplaire = exemplaire;
    }

    public Emprunt() {
    }
}
