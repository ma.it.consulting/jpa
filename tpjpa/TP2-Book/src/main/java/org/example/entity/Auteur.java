package org.example.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "auteur")
public class Auteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_a;

    @Column(length = 30)
    private String nom_a;

    @Column(length = 30)
    private String prenom_a;

    private Date date_naissance_a;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_p")
    private Pays pays;

    @ManyToMany(mappedBy = "auteurList")
    private List<Livre> livreList = new ArrayList<>();
    public Long getId_a() {
        return id_a;
    }

    public void setId_a(Long id_a) {
        this.id_a = id_a;
    }

    public String getNom_a() {
        return nom_a;
    }

    public void setNom_a(String nom_a) {
        this.nom_a = nom_a;
    }

    public String getPrenom_a() {
        return prenom_a;
    }

    public void setPrenom_a(String prenom_a) {
        this.prenom_a = prenom_a;
    }

    public Date getDate_naissance_a() {
        return date_naissance_a;
    }

    public void setDate_naissance_a(Date date_naissance_a) {
        this.date_naissance_a = date_naissance_a;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public List<Livre> getLivreList() {
        return livreList;
    }

    public void setLivreList(List<Livre> livreList) {
        this.livreList = livreList;
    }

    public Auteur(Long id_a, String nom_a, String prenom_a, Date date_naissance_a, Pays pays) {
        this.id_a = id_a;
        this.nom_a = nom_a;
        this.prenom_a = prenom_a;
        this.date_naissance_a = date_naissance_a;
        this.pays = pays;
    }

    public Auteur(String nom_a, String prenom_a, Date date_naissance_a, Pays pays) {
        this.nom_a = nom_a;
        this.prenom_a = prenom_a;
        this.date_naissance_a = date_naissance_a;
        this.pays = pays;
    }

    public Auteur() {
    }

    @Override
    public String toString() {
        return "Auteur{" +
                "id_a=" + id_a +
                ", nom_a='" + nom_a + '\'' +
                ", prenom_a='" + prenom_a + '\'' +
                ", date_naissance_a=" + date_naissance_a +
                ", pays=" + pays +
                '}';
    }
}
