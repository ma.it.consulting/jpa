package org.example.DAO;


import org.example.entity.TypeLivre;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoTypeLivreImpl implements DaoTypeLivreInterface {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public TypeLivre create_typelivre(TypeLivre t) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(t);

        em.getTransaction().commit();
        em.close();
        return t;
    }

    @Override
    public TypeLivre select_TypeLivreById(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        TypeLivre typeLivre = em.find(TypeLivre.class,id);

        em.getTransaction().commit();
        em.close();
        return typeLivre;
    }

    @Override
    public void delete_typelivre(TypeLivre t) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        TypeLivre typeLivre = em.find(TypeLivre.class,t.getId_t());

        em.remove(typeLivre);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update_typelivre(TypeLivre t) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        TypeLivre typeLivre = em.find(TypeLivre.class,t.getId_t());

        typeLivre.setLivreList(t.getLivreList());
        typeLivre.setLibelle_t(t.getLibelle_t());
    }
}
