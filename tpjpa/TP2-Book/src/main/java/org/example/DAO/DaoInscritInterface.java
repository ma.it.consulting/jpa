package org.example.DAO;

import org.example.entity.Auteur;
import org.example.entity.Inscrit;

public interface DaoInscritInterface {

    Inscrit create_inscrit(Inscrit i);

    Inscrit select_InscritById(Long id);

    void delete_inscrit(Inscrit i);

    void update_inscrit(Inscrit i);
}
