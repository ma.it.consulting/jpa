package org.example.DAO;

import org.example.entity.Pays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoPaysImpl implements  DaoPaysInterface{

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public Pays create_pays(Pays p) {
        EntityManager em =emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(p);

        em.getTransaction().commit();
        em.close();
        return p;
    }

    @Override
    public Pays select_PaysById(Long id) {
       EntityManager em = emf.createEntityManager();
       em.getTransaction().begin();

       Pays pays = em.find(Pays.class,id);

       em.getTransaction().commit();
       em.close();

       return pays;
    }

    @Override
    public void update_pays(Pays p) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Pays pays = em.find(Pays.class,p.getId_p());
        pays.setNom_p(p.getNom_p());
        pays.setAuteurList(p.getAuteurList());
        em.merge(pays);


        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete_pays(Pays p) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Pays pays = em.find(Pays.class,p.getId_p());
        em.remove(pays);

        em.getTransaction().commit();
        em.close();
    }
}
