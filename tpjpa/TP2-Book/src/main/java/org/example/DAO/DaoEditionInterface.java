package org.example.DAO;


import org.example.entity.Edition;

public interface DaoEditionInterface {
    Edition create_edition(Edition e);

    Edition select_EditionById(Long id);

    void delete_edition(Edition e);

    void update_edition(Edition e);
}
