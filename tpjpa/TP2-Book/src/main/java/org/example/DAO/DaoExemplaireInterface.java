package org.example.DAO;


import org.example.entity.Exemplaire;

public interface DaoExemplaireInterface {

    Exemplaire create_exemplaire(Exemplaire ex);

    Exemplaire select_ExemplaireById(String ref);

    void delete_exemplaire(Exemplaire ex);

    void update_exemplaire(Exemplaire ex);
}
