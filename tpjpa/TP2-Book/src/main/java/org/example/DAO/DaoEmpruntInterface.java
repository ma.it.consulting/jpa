package org.example.DAO;

import org.example.entity.Emprunt;

public interface DaoEmpruntInterface {

    Emprunt create_emprunt(Emprunt e);

    Emprunt select_EmpruntById(Long id);

    void delete_emprunt(Emprunt e);

    void update_emprunt(Emprunt e);
}
