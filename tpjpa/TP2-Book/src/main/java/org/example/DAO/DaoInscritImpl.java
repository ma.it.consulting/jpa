package org.example.DAO;

import org.example.entity.Inscrit;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoInscritImpl implements  DaoInscritInterface{

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public Inscrit create_inscrit(Inscrit i) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(i);
        em.getTransaction().commit();
        em.close();
        return i;
    }

    @Override
    public Inscrit select_InscritById(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Inscrit inscrit = em.find(Inscrit.class,id);
        em.getTransaction().commit();
        em.close();
        return inscrit;
    }

    @Override
    public void delete_inscrit(Inscrit i) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Inscrit inscrit = em.find(Inscrit.class,i.getId_i());
        em.remove(inscrit);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update_inscrit(Inscrit i) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Inscrit inscrit = em.find(Inscrit.class,i.getId_i());
        inscrit.setNom_i(i.getNom_i());
        inscrit.setPrenom_i(i.getPrenom_i());
        inscrit.setCp_i(i.getCp_i());
        inscrit.setEmail_i(i.getEmail_i());
        inscrit.setDate_naissance_i(i.getDate_naissance_i());
        inscrit.setEmpruntList(i.getEmpruntList());
        inscrit.setRue_i(i.getRue_i());
        inscrit.setTel_i(i.getTel_i());
        inscrit.setTel_portable_i(i.getTel_portable_i());
        inscrit.setVille_i(i.getVille_i());
        em.merge(inscrit);
        em.getTransaction().commit();
        em.close();
    }
}
