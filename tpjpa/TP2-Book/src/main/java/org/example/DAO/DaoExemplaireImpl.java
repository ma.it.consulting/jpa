package org.example.DAO;

import org.example.entity.Exemplaire;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoExemplaireImpl implements  DaoExemplaireInterface{

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public Exemplaire create_exemplaire(Exemplaire ex) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(ex);
        em.getTransaction().commit();
        em.close();
        return ex;
    }

    @Override
    public Exemplaire select_ExemplaireById(String ref) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Exemplaire exemplaire = em.find(Exemplaire.class,ref);
        em.getTransaction().commit();
        em.close();
        return exemplaire;
    }

    @Override
    public void delete_exemplaire(Exemplaire ex) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Exemplaire exemplaire = em.find(Exemplaire.class,ex.getRef_e());
        em.remove(exemplaire);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update_exemplaire(Exemplaire ex) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Exemplaire exemplaire = em.find(Exemplaire.class,ex.getRef_e());
        exemplaire.setEdition(ex.getEdition());
        exemplaire.setLivre(ex.getLivre());
        exemplaire.setEmpruntList(ex.getEmpruntList());
        em.merge(exemplaire);
        em.getTransaction().commit();
        em.close();
    }
}
