package org.example.DAO;

import org.example.entity.TypeLivre;

public interface DaoTypeLivreInterface {

    TypeLivre create_typelivre(TypeLivre t);

    TypeLivre select_TypeLivreById(Long id);

    void delete_typelivre(TypeLivre t);

    void update_typelivre(TypeLivre t);
}
