package org.example.DAO;


import org.example.entity.Livre;

public interface DaoLivreInterface {

    Livre create_livre(Livre b);

    Livre select_LivreById(Long id);

    void delete_livre(Livre b);

    void update_livre(Livre b);

}
