package org.example.DAO;

import org.example.entity.Auteur;

public interface DaoAuteurInterface {

    Auteur create_auteur(Auteur a);

    Auteur select_AuteurById(Long id);

    void delete_auteur(Auteur a);

    void update_auteur(Auteur a);
}
