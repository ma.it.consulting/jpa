package org.example.DAO;

import org.example.entity.Livre;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoLivreImpl implements  DaoLivreInterface{

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public Livre create_livre(Livre b) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(b);

        em.getTransaction().commit();
        em.close();
        return b;
    }

    @Override
    public Livre select_LivreById(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Livre livre = em.find(Livre.class,id);

        em.getTransaction().commit();
        em.close();
        return livre;
    }

    @Override
    public void delete_livre(Livre b) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Livre livre = em.find(Livre.class,b.getId_l());

        em.remove(livre);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update_livre(Livre b) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Livre livre = em.find(Livre.class,b.getId_l());
        livre.setTypeLivre(b.getTypeLivre());
        livre.setAnnee_l(b.getAnnee_l());
        livre.setAuteurList(b.getAuteurList());
        livre.setExemplaireList(b.getExemplaireList());
        livre.setTitre_l(b.getTitre_l());
        livre.setResume_l(b.getResume_l());

        em.merge(livre);

        em.getTransaction().commit();
        em.close();
    }
}
