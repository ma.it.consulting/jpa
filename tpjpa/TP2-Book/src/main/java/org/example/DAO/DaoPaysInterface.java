package org.example.DAO;


import org.example.entity.Pays;

public interface DaoPaysInterface {
    Pays create_pays(Pays p);

    Pays select_PaysById(Long id);

    void delete_pays(Pays p);

    void update_pays(Pays p);
}
