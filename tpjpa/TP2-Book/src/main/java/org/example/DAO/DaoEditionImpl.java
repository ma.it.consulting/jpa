package org.example.DAO;

import org.example.entity.Edition;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoEditionImpl implements DaoEditionInterface{

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public Edition create_edition(Edition e) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(e);

        em.getTransaction().commit();
        em.close();
        return e;
    }

    @Override
    public Edition select_EditionById(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Edition edition = em.find(Edition.class,id);
        em.getTransaction().commit();
        em.close();
        return edition;
    }

    @Override
    public void delete_edition(Edition e) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Edition edition = em.find(Edition.class,e.getId_ed());
        em.remove(edition);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update_edition(Edition e) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Edition edition = em.find(Edition.class,e.getId_ed());
        edition.setExemplaireList(e.getExemplaireList());
        edition.setNom_ed(e.getNom_ed());
        em.merge(edition);
        em.getTransaction().commit();
        em.close();
    }
}
