package org.example.DAO;

import org.example.entity.Emprunt;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoEmpruntImpl implements org.example.DAO.DaoEmpruntInterface {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public Emprunt create_emprunt(Emprunt e) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(e);
        em.getTransaction().commit();
        em.close();
        return e;
    }

    @Override
    public Emprunt select_EmpruntById(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Emprunt emprunt = em.find(Emprunt.class,id);
        em.getTransaction().commit();
        em.close();
        return emprunt;
    }

    @Override
    public void delete_emprunt(Emprunt e) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Emprunt emprunt = em.find(Emprunt.class,e.getId_em());
        em.remove(emprunt);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update_emprunt(Emprunt e) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Emprunt emprunt = em.find(Emprunt.class,e.getId_em());
        emprunt.setDate_em(e.getDate_em());
        emprunt.setExemplaire(e.getExemplaire());
        emprunt.setDelais_em(e.getDelais_em());
        emprunt.setInscrit(e.getInscrit());
        em.getTransaction().commit();
        em.close();
    }
}
