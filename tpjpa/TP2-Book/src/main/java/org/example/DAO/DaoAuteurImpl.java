package org.example.DAO;

import org.example.entity.Auteur;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoAuteurImpl implements DaoAuteurInterface {

    private  static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_book");

    @Override
    public Auteur create_auteur(Auteur a) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(a);

        em.getTransaction().commit();
        em.close();
        return a;
    }

    @Override
    public Auteur select_AuteurById(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Auteur auteur = em.find(Auteur.class,id);
        em.getTransaction().commit();
        em.close();
        return auteur;
    }

    @Override
    public void update_auteur(Auteur a) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Auteur auteur = em.find(Auteur.class,a.getId_a());
        auteur.setNom_a(a.getNom_a());
        auteur.setPrenom_a(a.getPrenom_a());
        auteur.setPays(a.getPays());
        auteur.setDate_naissance_a(a.getDate_naissance_a());
        auteur.setLivreList(a.getLivreList());
        em.merge(auteur);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete_auteur(Auteur a) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Auteur auteur = em.find(Auteur.class,a.getId_a());
        em.remove(auteur);

        em.getTransaction().commit();
        em.close();
    }
}
