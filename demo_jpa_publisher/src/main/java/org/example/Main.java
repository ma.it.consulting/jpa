package org.example;

import org.example.entity.Book;
import org.example.entity.BookPublisher;
import org.example.entity.Publisher;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Date;

public class Main {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_demo");

    public static void main(String[] args) {

        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Book b1 = new Book("Corentin va t'il rester?");
        Book b2 = new Book("On espere tous");
        Book b3 = new Book("Et il met de la musique");

        em.persist(b1);
        em.persist(b2);
        em.persist(b3);

        Publisher p1 = new Publisher("Corentin");
        Publisher p2 = new Publisher("Maximilien");

        em.persist(p1);
        em.persist(p2);

        BookPublisher bp = new BookPublisher(b1,p1,new Date());
        BookPublisher bp1 = new BookPublisher(b1,p2,new Date());
        BookPublisher bp3 = new BookPublisher(b2,p1,new Date());
        BookPublisher bp4 = new BookPublisher(b3,p1,new Date());
        BookPublisher bp5 = new BookPublisher(b3,p2,new Date());

        em.persist(bp);
        em.persist(bp1);
        em.persist(bp3);
        em.persist(bp4);
        em.persist(bp5);

        em.getTransaction().commit();
        em.close();
        emf.close();

    }
}