package org.example.entity;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class BookPublisherId implements Serializable {


    @Column(name="book_id")
    private Long bookId;


    @Column(name="publisher_id")
    private Long publisherId;

    public BookPublisherId(){

    }

    public BookPublisherId(Long bookId, Long publisherId) {
        this.bookId = bookId;
        this.publisherId = publisherId;
    }
}
