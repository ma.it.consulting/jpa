package org.example.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "publisher")
    private List<BookPublisher> bookPublisherList = new ArrayList<>();


    public Publisher() {

    }

    public Publisher(String name) {
        this.name = name;
    }
}
