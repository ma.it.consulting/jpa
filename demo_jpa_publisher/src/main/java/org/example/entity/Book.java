package org.example.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String titre;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BookPublisher> bookPublisherList = new ArrayList<>();

    public Book(){

    }

    public Book(String titre) {
        this.id = id;
        this.titre = titre;
    }
}
