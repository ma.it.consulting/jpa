package org.example.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;


import org.bson.Document;
import org.example.utils.MongoConnection;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

public class DaoDemo {


    public static void createCollection() {

        MongoClient mongoclient = new MongoClient("localhost", 27017);

        MongoDatabase db = mongoclient.getDatabase("demoJava");

        System.out.println("Connect to db successfully ");

        db.createCollection("collectionJava");

        System.out.println("creation de la DB");

    }

    public static void insertDocumentCollection() {

        MongoClient mongoclient = new MongoClient("localhost", 27017);

        MongoDatabase db = mongoclient.getDatabase("demoJava");

        MongoCollection<Document> collection = db.getCollection("collectionJava");

        Document document = new Document("name", "Louis").append("info", "Il est gentil").append("_id", "0987655");

        List<Document> liste = new ArrayList<Document>();
        liste.add(document);
        collection.insertMany(liste);

    }


    public static void getAllDocument() {

        MongoClient mongoclient = new MongoClient("localhost", 27017);

        MongoDatabase db = mongoclient.getDatabase("demoJava");

        MongoCollection<Document> collection = db.getCollection("collectionJava");

        FindIterable<Document> findIterable = collection.find();

        MongoCursor<Document> mongoCursor = findIterable.iterator();

        while (mongoCursor.hasNext()) {
            System.out.println(mongoCursor.next());
        }
    }

    public static void getOneDocument() {

        MongoClient mongoclient = new MongoClient("localhost", 27017);

        MongoDatabase db = mongoclient.getDatabase("demoJava");

        MongoCollection<Document> collection = db.getCollection("collectionJava");

        BasicDBObject query = new BasicDBObject("_id", "0987655");

        MongoCursor doc = (MongoCursor) collection.find(query);


    }


}
