package org.example.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name="client")
public class Client implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="num_client")
    private Long numClient;
    
    @Column(name="nom_client",nullable = false)
    private String nomClient;
    
    @Column(name="prenom_client",nullable = false)
    private String prenomClient;

    @Column(name="date_naissance",nullable = false)
    @Temporal(TemporalType.DATE) 
    private Date dateNaissance;
    
    @ManyToMany(mappedBy = "clients", cascade=CascadeType.DETACH)
    private Set<Account> accounts = new HashSet<Account>();

    public Client() {
    }

    public Long getNumClient() {
        return numClient;
    }

    public void setNumClient(Long numClient) {
        this.numClient = numClient;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getPrenomClient() {
        return prenomClient;
    }

    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }

    public Date getDateNaiss() {
        return dateNaissance;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaissance = dateNaiss;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Account account) {
        this.accounts.add(account);
    }
    
    
    
    
    
    
}
